package echo;

import static org.junit.jupiter.api.Assertions.*;

import java.io.Console;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import echo.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;



class ControllerTest {

    private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }
//     @Test
//    void getCobrancaTest() {
//        HttpResponse response = Unirest.get("http://localhost:7010/cobranca/1").asString();
//        assertEquals(200, response.getStatus());
//    }
//
//    @Test
//    void getEchoTest() {
//        HttpResponse response = Unirest.get("http://localhost:7010/Artur").asString();
//        assertEquals(200, response.getStatus());
//        assertEquals("Artur Artur Artur",response.getBody());
//    }
//
//    @Test
//    void getRootTest() {
//        HttpResponse response = Unirest.get("http://localhost:7010/").asString();
//        assertEquals(200, response.getStatus());
//        assertEquals("Isto e um eco, digite algo a mais no caminho",response.getBody());
//    }
    @Test
    void getCobarancaTest() {    	         	
        HttpResponse response = Unirest.get("https://unirio-g23.herokuapp.com/cobranca/5fc808e4-d59d-4556-8ee7-b2d754b8cdf1").asString();
        assertEquals(200, response.getStatus());          
    }
    //Minha rota nao Funciona
//    @Test
//    void postEnviarEmailTest() {    	         	
//       HttpResponse<JsonNode> response = Unirest.post("https://unirio-g23.herokuapp.com/enviarEmail").body("{\"email\":\"rafael.souza@uniriotec.br\", \"mensagem\":\"Teste de Email Projeto PM\"}").asJson();
//       assertEquals(200, response.getStatus());     
//    }

    @Test
    void postFilaCobrancaTest() {    	         	
    	HttpResponse<JsonNode> response = Unirest.post("https://unirio-g23.herokuapp.com/filaCobranca").body("{\"valor\":\"120\", \"ciclista\":\"142083d0-48d6-4751-9afa-76ec5d6fb1ee\"}").asJson();
    	assertEquals(200, response.getStatus());     
    }
    
    @Test
    void postVerificarCartaoTest() {    	         	
        HttpResponse<JsonNode> response = Unirest.post("https://unirio-g23.herokuapp.com/validaCartaoDeCredito").body("{\"numero\":\"4539064125731891\", \"validade\":\"7/2044\", \"nomeTitular\": \"Matthew Lewis\", \"cvv\":\"149\"}").asJson();
   	 assertEquals(200, response.getStatus());     
    }
    
    @Test
    void postCobranca() {    	         	
        HttpResponse<JsonNode> response = Unirest.post("https://unirio-g23.herokuapp.com/cobranca").body("{\"valor\":\"1\", \"ciclista\":\"d61a09f0-3b04-4b66-8c6d-56e5291d89ce\"}").asJson();
   	 assertEquals(200, response.getStatus());     
    }


}
