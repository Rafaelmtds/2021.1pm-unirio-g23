package echo;

import io.javalin.http.Context;
import io.javalin.Javalin;
//import io.javalin.plugin.openapi.OpenApiOptions;
//import io.javalin.plugin.openapi.OpenApiPlugin;
//import io.javalin.plugin.openapi.ui.ReDocOptions;
//import io.javalin.plugin.openapi.ui.SwaggerOptions;
//import io.swagger.v3.oas.models.info.Info;
import static io.javalin.apibuilder.ApiBuilder.*;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class Controller {
	
	private Controller(){}
        
    public static void getEcho(Context ctx) {
        String echo = ctx.pathParam("echo");
        ctx.result(echo +" "+ echo +" "+ echo);
        ctx.status(200);
    }

    public static void getRoot(Context ctx) {
        ctx.status(200);
        ctx.result("Isto e um eco, digite algo a mais no caminho testando alteração");
    }
    

}
