package echo.util;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;

import cartao.CartaoDeCreditoController;
import echo.Controller;
import email.EmailController;
import cobranca.CobrancaController;



public class JavalinApp {
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                	 path("/:echo", () -> get(Controller::getEcho));
                     path("/", ()-> get(Controller::getRoot));
                    path("/enviarEmail", ()-> post(EmailController::sendEmail));
                    path("/cobranca/:idCobranca", ()-> get(CobrancaController::getCobranca));
                    path("/filaCobranca", () -> post(CobrancaController::postFilaCobranca));
                    path("/cobranca", () -> post(CobrancaController::fazerCobranca));
                    path("/ciclista/:id", () -> get(CobrancaController::pesquisarCiclista));
                    path("/validaCartaoDeCredito", () -> post(CartaoDeCreditoController::validaCartaoCredito));
                    

                    });
                    


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
