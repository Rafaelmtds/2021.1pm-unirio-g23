package cartao;

public class CartaoDeCreditoForm {

	public String cvv;
	public String nomeTitular;
	public String numero;
	public String validade;
	public String id;

	public CartaoDeCreditoForm( String nomeTitular,  String numero, String validade, String cvv){
		this.cvv = cvv;
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
	}
	public CartaoDeCreditoForm(){
		
	}
	public CartaoDeCredito desserializar(){
		return new CartaoDeCredito(this.nomeTitular,this.numero,this.validade, this.cvv);
	}

}
