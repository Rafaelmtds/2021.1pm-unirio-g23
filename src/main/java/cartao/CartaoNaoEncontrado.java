package cartao;

public class CartaoNaoEncontrado extends Exception {
	public CartaoNaoEncontrado(String mensagem) {
		super(mensagem);
	}

}
