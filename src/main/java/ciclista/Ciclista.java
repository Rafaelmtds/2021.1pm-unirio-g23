package ciclista;

import java.util.ArrayList;
import cartao.CartaoDeCredito;
import cartao.CartaoNaoEncontrado;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

public class Ciclista {
	private String nome;
	private String id;
	private CartaoDeCredito cartao;
	public Ciclista() {
		
	}
	public Ciclista(String id, String nome, CartaoDeCredito cartao) {
		this.id = id;
		this.nome = nome;
		this.cartao = cartao;
	}
	public Ciclista(String id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CartaoDeCredito getCartao() {
		return cartao;
	}
	public void setCartao(CartaoDeCredito cartao) {
		this.cartao = cartao;
	}
	public static ArrayList<Ciclista> retornarCiclista() throws CartaoNaoEncontrado {
		ArrayList<Ciclista> ciclistas = new ArrayList<Ciclista>();
		ciclistas.add(new Ciclista("3fa85f64-5717-4562-b3fc-2c963f66afa1", "Rafael Mota de Souza", CartaoDeCredito.consultarCartoes("Rafael Mota de Souza")));
		return ciclistas;
	}
	public static Ciclista consultarCiclistaPeloId(String id) throws CartaoNaoEncontrado, CiclistaNaoEncontrado{
		ArrayList<Ciclista> ciclistas = retornarCiclista();
		Ciclista ciclista = null;
		for(int i = 0; i< ciclistas.size(); i++) {
			if(ciclistas.get(i).getId().equals(id)) {
				ciclista = ciclistas.get(i);
				break;
			}
		}
		if(ciclista != null) {
			return ciclista;
		}
		else {
			throw new CiclistaNaoEncontrado("Ciclista nao encontrado");
		}
	}
	
	public Ciclista retornarCiclistaInteracao(String id) {
    	//Integração com EndPoint de Aluno do Período passado G3
		HttpResponse<JsonNode> response = Unirest.get("https://grupo3-aluguel.herokuapp.com/ciclista/" + id).header("accept", "application/json").asJson();
    	// Integração com Grupo deste Período G11 - rota não funciona
//		HttpResponse<JsonNode> response = Unirest.get("https://trabalho-pm-henrique.herokuapp.com/ciclista/" + id).header("accept", "application/json").asJson();
		System.out.println(response.getStatus());
		JSONObject json = response.getBody().getObject();
		System.out.println(json);
		//Formatação do EndPoint de Aluno do Período passado G3
		CartaoDeCredito cartao = new CartaoDeCredito(json.getJSONObject("meioDePagamento").getString("nomeTitular"),json.getJSONObject("meioDePagamento").getString("numero"), json.getJSONObject("meioDePagamento").getString("validade"), json.getJSONObject("meioDePagamento").getString("cvv"));
		//Formatação do EndPoint deste período G11
		//Como endpoint nao estava funcionando nao formatei
		Ciclista ciclista = new Ciclista(json.getString("id"), json.getString("nome"), cartao);
		return ciclista;		
	}




	public static Ciclista consultarCiclistaPeloNome(String nome) throws CartaoNaoEncontrado, CiclistaNaoEncontrado{
		ArrayList<Ciclista> ciclistas = retornarCiclista();
		Ciclista ciclista = null;
		for(int i = 0; i< ciclistas.size(); i++) {
			if(ciclistas.get(i).getNome().equals(nome)) {
				ciclista = ciclistas.get(i);
				break;
			}
		}
		if(ciclista != null) {
			return ciclista;
		}
		else {
			throw new CiclistaNaoEncontrado("Ciclista nao encontrado");
		}
	}

}
