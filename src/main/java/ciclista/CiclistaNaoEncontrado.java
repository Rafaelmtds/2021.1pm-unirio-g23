package ciclista;

public class CiclistaNaoEncontrado extends Exception {
	public CiclistaNaoEncontrado(String mensagem) {
		super(mensagem);
	}
}
