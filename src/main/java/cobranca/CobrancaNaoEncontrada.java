package cobranca;

public class CobrancaNaoEncontrada extends Exception {
	public CobrancaNaoEncontrada(String mensagem) {
		super(mensagem);
	}

}
