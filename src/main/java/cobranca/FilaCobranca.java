package cobranca;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;


public class FilaCobranca {

	private static Queue<Cobranca> filaCobranca = new LinkedList<>();
	
	

	public Queue<Cobranca> getFilaCobranca() {
		return filaCobranca;
	}

	public static void inserir(Cobranca cobranca) {
		filaCobranca.add(cobranca);
	}

	public void setFilaCobranca(Cobranca cobranca) {
		SecureRandom rand = new SecureRandom();
		Cobranca cobrancaFormatada = cobranca;
		cobrancaFormatada.setId(rand.nextInt(1000)+"");;
		cobrancaFormatada.setStatus(Status.PENDENTE);
		cobrancaFormatada.setHoraSolicitacao(cobrancaFormatada.obterDataHora());	
		this.filaCobranca.add(cobrancaFormatada);
	}

	


	
	



}
