package cobranca;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Objects;

import cartao.CartaoDeCreditoController;
import cartao.CartaoNaoEncontrado;
import ciclista.Ciclista;
import ciclista.CiclistaNaoEncontrado;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import cobranca.Cobranca;
import integracoesPagamento.IntegracaoPagamento;



public class CobrancaController {

	public static void getCobranca(Context ctx){
	      String id = Objects.requireNonNull(ctx.pathParam("idCobranca"));		 	       
	        Cobranca cobranca = null;
			try {
				cobranca = Cobranca.consultarCobranca(id);
				  if (cobranca == null) {
			            ctx.status(400);
			        } else {
			        	ctx.json(cobranca);
			        	ctx.status(200);
			        	
			            		
			        }
			} catch (CobrancaNaoEncontrada e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	      	
	}
	public static void pesquisarCiclista(Context ctx) {
	      String id = Objects.requireNonNull(ctx.pathParam("idCiclista"));		 	       
		Ciclista ciclista = new Ciclista();
//		ciclista = ciclista.retornarCiclistaInteracao(id);
		System.out.println(ciclista);
    	ctx.status(200);
	}
	
	
	
	public static void fazerCobranca(Context ctx) throws CartaoNaoEncontrado, CiclistaNaoEncontrado {
		CobrancaForm cobrancaFormatada = ctx.bodyAsClass(CobrancaForm.class);
    	Cobranca cobranca = cobrancaFormatada.desserializarPagamento();
	    	try {
	    		
	        	Ciclista ciclistaNovaCobranca = new Ciclista();
	        	//Simulação para Recuperar Ciclista
//	        		ciclistaNovaCobranca = ciclistaNovaCobranca.consultarCiclistaPeloId(cobrancaFormatada.ciclistaId);
	        	//Preparação para Recuperar Ciclista Por EndPoint
	    		ciclistaNovaCobranca = ciclistaNovaCobranca.retornarCiclistaInteracao(cobrancaFormatada.ciclista);
	    		System.out.println(ciclistaNovaCobranca);
	    		IntegracaoPagamento enviadora = new IntegracaoPagamento(cobranca, ciclistaNovaCobranca);
	    	    	HttpResponse<JsonNode> response = enviadora.enviarCobranca();
	    	    	
	    	    	
	    	    	if (response.getStatus()  == 200 || response.getStatus()  == 201) {
	    	    		ctx.json(cobranca); 
	    	    		ctx.status(response.getStatus());
	    	             return;
	    	         } 
	    	    	else {
	    	    	 ctx.status(response.getStatus());
	    	    	 		return;
	    		    }

			} catch (Exception e) {
				e.printStackTrace();
			}
    	
	 }


	
	public static void postFilaCobranca(Context ctx) {
		CobrancaForm cobrancaJson = ctx.bodyAsClass(CobrancaForm.class);
		
    	Cobranca cobranca = cobrancaJson.desserializar();
		FilaCobrancaDAO filaCobrancas = new FilaCobrancaDAO();	   
		try {  	
	    	FilaCobranca filaCobranca = filaCobrancas.retornarFilaCobrancas();
	    	filaCobranca.setFilaCobranca(cobranca);
	    	ctx.json(filaCobranca);
	    	ctx.status(200);
	    	
		}catch(Exception ex) {
			
			ctx.status(402);
		}


	}
	
	


}





