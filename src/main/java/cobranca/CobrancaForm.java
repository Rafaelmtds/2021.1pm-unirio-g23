package cobranca;
import java.security.SecureRandom;
import java.util.UUID;

public class CobrancaForm {
	public String id;
	public Status status;
	public String horaSolicitacao;
	public String horaFinalizacao;
	public String ciclista;
	public int valor;
	

	CobrancaForm(Status status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclista){
		SecureRandom rand = new SecureRandom();
		this.id = rand.nextInt(1000)+"";
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclista = ciclista;
	}
	CobrancaForm(String id, Status status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclista){
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclista = ciclista;
	}
	public CobrancaForm(String ciclista,  int valor){
		this.id = ciclista;
		this.valor = valor;
		this.ciclista = ciclista;
	}
	public CobrancaForm() {
		
	}
	public Cobranca desserializar(){
		return new Cobranca(this.id, this.status, this.horaFinalizacao, this.horaFinalizacao, this.valor, this.ciclista);
	}
	public Cobranca desserializarPagamento(){
		return new Cobranca(this.ciclista, this.valor);
	}
}
