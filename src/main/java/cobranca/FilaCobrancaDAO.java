package cobranca;

import java.util.ArrayList;

public class FilaCobrancaDAO {
	
	private FilaCobranca filaCobranca = new FilaCobranca();
	

    FilaCobrancaDAO() {
    }
    
    Cobranca removerCobranca() {
    	return filaCobranca.getFilaCobranca().remove();
    }

	public FilaCobranca getFilaCobranca() {
		return filaCobranca;
	}
 
  	public FilaCobranca retornarFilaCobrancas() {
  		FilaCobranca filaCobrancas = new FilaCobranca();
  		filaCobrancas.getFilaCobranca().add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa4", Status.PENDENTE, "15-01-2021-12:00", "15-01-2021-13:00", 30, "210")) ;
  		filaCobrancas.getFilaCobranca().add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", Status.PENDENTE, "17-04-2021-13:00", "12-04-2021-13:05", 30, "210")) ;
  		filaCobrancas.getFilaCobranca().add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa3", Status.CANCELADA, "17-04-2021-13:00", "", 35, "124"));
  		filaCobrancas.getFilaCobranca().add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa1", Status.PAGA, "17-04-2021-13:00", "", 5, "532"));
  		return filaCobrancas;  		
  	}

}


