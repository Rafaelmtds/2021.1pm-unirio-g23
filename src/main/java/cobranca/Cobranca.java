package cobranca;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Queue;
import java.security.SecureRandom;
import org.eclipse.jetty.util.ajax.JSON;
import ciclista.Ciclista;



public class Cobranca {
	private static final String MerchantId = "abcbe5bc-890a-4633-873a-df2cd1e4bec1";
	private static final String MerchantKey = "XYOSMOPFAKGDSLRUHMUGICXLZFIMHRXNMPFEBMGS";
	private int valor;
	private String ciclista;
	private String id;
	private Status status;
	private String horaSolicitacao;
	private String horaFinalizacao;
	
	public Cobranca() {
		
	}
	public Cobranca(String ciclista, int valor ) {
		SecureRandom rand = new SecureRandom();
		this.id = rand.nextInt(1000)+"";
		this.valor = valor;
		this.ciclista = ciclista;
		this.status = Status.PENDENTE;
		this.horaSolicitacao = obterDataHora();
		this.horaFinalizacao = ""; 
	}
	public Cobranca(String id, Status status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclista) {
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclista = ciclista;
	}	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getHoraSolicitacao() {
		return horaSolicitacao;
	}
	public void setHoraSolicitacao(String horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}
	public String getHoraFinalizacao() {
		return horaFinalizacao;
	}
	public void setHoraFinalizacao(String horaFinalizacao) {
		this.horaFinalizacao = horaFinalizacao;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public String getCiclista() {
		return ciclista;
	}
	public void setCiclista(String ciclista) {
		this.ciclista = ciclista;
	}
	public static void cobrarCiclista() {
		
	}
	public static Cobranca consultarCobranca(String id) throws CobrancaNaoEncontrada {
		ArrayList<Cobranca> cobrancas = retornarCobranca();
		Cobranca cobranca = null;
		for(int i = 0; i <cobrancas.size(); i++) {
			if(cobrancas.get(i).getId().equals(id)) {
				cobranca = cobrancas.get(i);
				break;
			}
		}
		if(cobranca != null) {
			return cobranca;
		}
		else {
			throw new CobrancaNaoEncontrada("Nao foi possivel encontrar a cobranca");
		}
	}
	public static ArrayList<Cobranca> retornarCobranca() {
		ArrayList<Cobranca> BDcobrancas = new ArrayList<Cobranca>();
		BDcobrancas.add(new Cobranca("5fc808e4-d59d-4556-8ee7-b2d754b8cdf1", Status.PENDENTE, "17-04-2021-13:00", "17-04-2021-13:05", 30, "3fa85f64-5717-4562-b3fc-2c963f66afa6"));
		BDcobrancas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa2", Status.PAGA, "17-04-2021-13:07", "17-04-2021-13:09", 5, "3fa85f64-5717-4562-b3fc-2c963f66afa6"));
		BDcobrancas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa3", Status.CANCELADA, "17-04-2021-13:00", "", 35, "3fa85f64-5717-4562-b3fc-2c963f66afa5"));
		BDcobrancas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa4", Status.PAGA, "17-04-2021-13:00", "", 5, "3fa85f64-5717-4562-b3fc-2c963f66afa6"));
		return BDcobrancas;
	}

	public static String obterDataHora() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return formatter.format(date);
	}
	public Cobranca desserializar(){
		return new Cobranca(this.id, this.status, this.horaFinalizacao, this.horaFinalizacao, this.valor, this.id);
	}

	
}
