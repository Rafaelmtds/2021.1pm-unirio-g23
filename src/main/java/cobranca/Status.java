package cobranca;

public enum Status {
	PENDENTE, PAGA, FALHA, CANCELADA, OCUPADA
}
