package integracoesPagamento;

import com.fasterxml.jackson.core.JsonProcessingException;

import cartao.CartaoDeCredito;
import ciclista.Ciclista;
import cobranca.Cobranca;
import kong.unirest.HttpRequestWithBody;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.ObjectMapper;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

	public class IntegracaoPagamento {
	private static final String MerchantId = "b2302192-0e37-4d11-9e09-395038a45aad";
	private static final String MerchantKey = "TXMEFITTYWTAHPKAGYNEBRQUMFCVRAWWHHUPVCCC";
	private static final String requisicaoCielo = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/";
	private static final String consultaCielo =	"https://apiquerysandbox.cieloecommerce.cielo.com.br";
	private Cobranca cobranca;
	private Ciclista ciclista;
	
	
	public IntegracaoPagamento (){
		
	}

	
	public IntegracaoPagamento (Cobranca cobranca, Ciclista ciclista){
		this.cobranca = cobranca;
		this.ciclista = ciclista;
	}
	public HttpResponse<JsonNode> enviarCobranca() {
		EstruturaPagamento transacao = new EstruturaPagamento("1");
		transacao.Customer.Name = this.ciclista.getNome();
		transacao.Payment.setAmount(cobranca.getValor());
		transacao.Payment.setInstallments(1);
		transacao.Payment.setType("CreditCard");
		transacao.Payment.getCreditCard().setBrand("Visa");
		transacao.Payment.getCreditCard().setCardNumber("0000.0000.0000.0001");
		transacao.Payment.getCreditCard().setExpirationDate("10/2025");
		transacao.Payment.getCreditCard().setSecurityCode(ciclista.getCartao().getCvv());
		HttpResponse<JsonNode>response = Unirest.post(requisicaoCielo).header("Content-Type", "application/json").header("MerchantKey", MerchantKey).header("MerchantId", MerchantId).body(transacao).asJson();
		System.out.println(response.getBody());
		System.out.println(response.getStatus());
		return response;
	}
	


}
