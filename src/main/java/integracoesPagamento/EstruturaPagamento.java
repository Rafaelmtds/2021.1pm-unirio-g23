package integracoesPagamento;

import cartao.CartaoDeCredito;

public class EstruturaPagamento {


	public EstruturaPagamento(String MerchantOrderId){
		this.Customer = new Customer();
		this.MerchantOrderId = MerchantOrderId;
		this.Payment = new PaymentFormat();
	}

	public String MerchantOrderId;
	public Customer Customer;
	public PaymentFormat Payment;
	
	public class Customer{
	      public String Name;
	}


}